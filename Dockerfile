FROM alpine:3.6

RUN apk --update add ruby

ADD http_server.rb /

CMD ["ruby","http_server.rb"]