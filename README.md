# elb-alb-healthcheck
🐳 Returns 200 - Intended to use with ELB/ALB health checks

## Use-case:
When using a frontend ALB/ELB and Traefik in a Docker Swarm environment, ALB/ELB health checks fail when configured for HTTP(S), due to services listening on a specific Host header.

This simple Alpine container with Ruby installed starts up a web server and returns 200 OK to any web request sent to it. Handy if the only health check you care about is the path from your frontend to your swarm.

[Dockerhub](https://hub.docker.com/r/stroebs/elb-alb-healthcheck/)
